#include <stdio.h>
#include <string.h>

#define MAX_FILES 10

struct File {
    char name[20];
    int size;
};

struct SingleLevelDirectory {
    struct File files[MAX_FILES];
    int fileCount;
};

void initializeSingleLevelDirectory(struct SingleLevelDirectory *directory) {
    directory->fileCount = 0;
}

void addFile(struct SingleLevelDirectory *directory, const char *name, int size) {
    if (directory->fileCount < MAX_FILES) {
        strcpy(directory->files[directory->fileCount].name, name);
        directory->files[directory->fileCount].size = size;
        directory->fileCount++;
        printf("File '%s' added successfully.\n", name);

        // Create a physical file on the file system
        FILE *file = fopen(name, "w");
        fclose(file);
    } else {
        printf("Directory is full. Cannot add more files.\n");
    }
}

void displaySingleLevelDirectory(struct SingleLevelDirectory *directory) {
    printf("Single Level Directory:\n");
    for (int i = 0; i < directory->fileCount; i++) {
        printf("File Name: %s, Size: %d KB\n", directory->files[i].name, directory->files[i].size);
    }
}

int main() {
    struct SingleLevelDirectory directory;
    initializeSingleLevelDirectory(&directory);

    addFile(&directory, "file1.txt", 100);
    addFile(&directory, "file2.txt", 150);
    addFile(&directory, "file3.txt", 200);

    displaySingleLevelDirectory(&directory);

    return 0;
}

