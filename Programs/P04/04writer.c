#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>

#define FIFO_NAME "/tmp/myfifo"

int main() {
    // Create the FIFO (named pipe)
    if (mkfifo(FIFO_NAME, 0666) == -1) {
        perror("Error creating FIFO");
        exit(EXIT_FAILURE);
    }

    int fd;
    // Open the FIFO for writing
    if ((fd = open(FIFO_NAME, O_WRONLY)) == -1) {
        perror("Error opening FIFO for writing");
        exit(EXIT_FAILURE);
    }

    // Writer sends five bash commands to the FIFO
    for (int i = 0; i < 5; ++i) {
        char command[100];
        printf("Enter a bash command: ");
        fgets(command, sizeof(command), stdin);

        write(fd, command, sizeof(command));

        // Sleep to simulate delay between commands
        sleep(1);
    }
	//delete the FIFO
	remove(FIFO_NAME);	
    // Close the FIFO
    close(fd);
    exit(EXIT_SUCCESS);
}

